'''
Custom Jinja2 template filters.
'''

from webhelpers import text


def datetimeformat(value, format='%x'):
    '''
    Format the given datetime according to the given format.
    
    @param value datetime instance
    '''
    return value.strftime(format)


def pluralize(value, singular, plural, with_number=True):
    '''
    @param value Number of items
    @param singular Singular form of items
    @param plural Plural form of items
    '''
    return text.plural(value, singular, plural, with_number)
