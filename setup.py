from setuptools import setup, find_packages

requires = [
    'pyramid',
    'pyramid_debugtoolbar',
    'pyramid_tm',
    'SQLAlchemy',
    'transaction',
    'waitress',
    'WebTest',
    'zope.sqlalchemy',
    ]

setup(name='stock-forecaster',
      packages=find_packages(),
      include_package_data=True,
      zip_safe=False,
      test_suite='stockforecaster',
      install_requires=requires,
      entry_points={
          'paste.app_factory': ['main=stockforecaster:main'],
          'console_scripts': [
              'stockforecaster_initializedb=stockforecaster.scripts.initializedb:main',
              ],
          },
      )