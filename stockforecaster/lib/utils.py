'''
Miscellaneous helper functions.
'''

import datetime


def get_next_business_date(d):
    '''
    Naively get the next business day after the given day. This does not take 
    into account any holidays.
    '''
    return d + datetime.timedelta(days=7 - d.weekday() if d.weekday() > 3 else 1)


def split_cslist(cslist):
    '''
    Split a comma separated list by both newlines and commas.
    '''
    items = []
    for line in cslist.splitlines():
        for item in line.split(','):
            item.strip() and items.append(item.strip())
    return items


def settings_from_prefix(settings, prefix):
    '''
    Return a dictionary of settings all containing the same key prefix, with the
    prefix removed from the keys.
    '''
    data = {}
    for key in settings:
        if key.startswith(prefix):
            data[key.replace(prefix, '')] = settings[key]
    return data


class Storage(dict):
    '''
    Generic container class.
    '''

    def __getattr__(self, key):
        try:
            return self[key]
        except KeyError as e:
            raise AttributeError(e)

    def __setattr__(self, key, value):
        self[key] = value

    def __delattr__(self, key):
        try:
            del self[key]
        except KeyError as e:
            raise AttributeError(e)
