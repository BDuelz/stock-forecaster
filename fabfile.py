import os
from fabric.api import cd, env, run, settings, sudo, task
from fabric.contrib.files import upload_template
from fabric.utils import _AttributeDict as AttributeDict


PRODUCTION, DEVELOPMENT = range(2)

# Put custom configuration in its own namespace
env.config = AttributeDict()
env.config.project_name = 'stock-forecaster'
env.config.host_name = 'orion'

# Needed directories
env.config.dirs = {'app': os.path.join('~', 'apps', env.config.project_name)}
env.config.dirs['cache'] = os.path.join(env.config.dirs['app'], 'data', 'cache')
env.config.dirs['template_cache'] = os.path.join(env.config.dirs['cache'], 'templates')
env.config.dirs['databases'] = os.path.join(env.config.dirs['app'], 'data', 'databases')
env.config.dirs['logs'] = os.path.join(env.config.dirs['app'], 'data', 'logs')
env.config.dirs['sessions'] = os.path.join(env.config.dirs['app'], 'data', 'sessions')

# Needed system packages
env.config.packages = (
    'build-essential',
    'git',
    'ipython',
    'libatlas-dev',
    'monit', 
    'nginx',
    'postgresql-9.1', 
    'postgresql-server-dev-9.1',
    'python2.7', 
    'python2.7-dev',
    'python-pip',
    'python-sklearn',
    'redis-server',
)

# DB configuration
env.config.db_name = env.config.project_name
env.config.db_user = 'abdul'
env.config.db_pass = 'RuEnG2013'


@task
def vagrant():
    from vagrant import Vagrant
    v = Vagrant()
    v.up()
    env.hosts = [v.user_hostname_port()]
    env.key_filename = v.keyfile()
    env.disable_known_hosts = True


@task
def development():
    env.config.ENVIRONMENT = DEVELOPMENT
    env.config.config_file = 'development.ini'


@task
def production():
    env.config.ENVIRONMENT = PRODUCTION
    env.config.config_file = 'production.ini'


@task
def deploy():
    bootstrap()
    install_app()
    setup_database()
    restart_all()


@task
def bootstrap():
    create_directories(env.config.dirs.values())
    setup_host_config()
    install_packages(env.config.packages)
    setup_config()


@task
def install_packages(packages):
    update_packages()
    if not isinstance(packages, basestring):
        packages = ' '.join(packages)
    sudo('apt-get -f -m -y install {packages}'.format(packages=packages))


@task
def install_app():
    with cd(env.config.dirs['app']):
        sudo('python setup.py develop && pip install -r requirements.txt')


@task
def setup_config():
    setup_host_config()
    setup_nginx_config()
    setup_monit_config()


@task
def setup_host_config():
    context = {'host_name': env.config.host_name,
               'host_ip': env.host}
    kwargs = dict(context=context, use_jinja=True, use_sudo=True, backup=False, mode=0644)
    upload_template('conf/hosts', '/etc/hosts', **kwargs)
    upload_template('conf/hostname', '/etc/hostname', **kwargs)
    sudo('service hostname restart')


@task
def setup_nginx_config():
    dirs = ('/etc/nginx', '/etc/nginx/conf.d')
    with settings(warn_only=True):
        sudo('mkdir -p {dirs}'.format(dirs=' '.join(dirs)))
    context = {'package_root': expand_path(os.path.join(env.config.dirs['app'], 'stockforecaster')),
               'error_log': expand_path(os.path.join(env.config.dirs['logs'], 'error.log'))}
    kwargs = dict(use_sudo=True, backup=False, mode=0644)
    upload_template('conf/nginx/nginx.conf', '/etc/nginx/nginx.conf', **kwargs)
    upload_template('conf/nginx/conf.d/stock-forecaster.conf', '/etc/nginx/conf.d/stock-forecaster.conf', 
                    context, use_jinja=True, **kwargs)
    sudo('rm -f /etc/nginx/sites-enabled/default')


@task
def setup_monit_config():
    dirs = ('/etc/monit', '/etc/monit/conf.d')
    with settings(warn_only=True):
        sudo('mkdir -p {dirs}'.format(dirs=' '.join(dirs)))
    context = {'app_dir': expand_path(env.config.dirs['app']),
               'config_file': env.config.config_file,
               'is_production': env.config.ENVIRONMENT is PRODUCTION}
    kwargs = dict(context=context, use_jinja=True, use_sudo=True, backup=False, mode=0644)
    upload_template('conf/monit/monitrc', '/etc/monit/monitrc', **kwargs)
    upload_template('conf/monit/conf.d/stock-forecaster.conf', '/etc/monit/conf.d/stock-forecaster.conf', **kwargs)
    sudo('chown root /etc/monit/monitrc')


@task
def setup_database():
    sudo('service postgresql restart')
    with settings(warn_only=True):
        sudo('psql -c "CREATE USER {db_user} WITH ENCRYPTED PASSWORD \'{db_pass}\'"'.format(**env.config), user='postgres')
        sudo('createdb {db_name}'.format(**env.config), user='postgres')
    with cd(env.config.dirs['app']):
        run('stockforecaster_initializedb {config_file}'.format(**env.config))


@task
def restart_all():
    sudo('service monit restart')
    sudo('monit restart all monitor all')


@task
def info():
    run('hostname -I')


@task
def monit_status():
    sudo('monit status')


def create_directories(dirs):
    if not isinstance(dirs, basestring):
        dirs = ' '.join(dirs)
    with settings(warn_only=True):
        run('mkdir -p {dirs}'.format(dirs=dirs))

def update_packages():
    sudo('apt-get -f -m -y update')


def expand_path(path):
    return run('readlink -f {path}'.format(path=path))
