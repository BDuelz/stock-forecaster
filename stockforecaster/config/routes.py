'''
Route configuration.
'''

def includeme(config):
    config.add_route('company.list', '/')
    config.add_route('company.view', '/company/{id:\d+}')
    config.add_route('company.add', '/company/add')
    config.add_route('company.remove', '/company/remove/{id:\d+}')
    config.add_route('fetch_quotes', '/fetch_quotes')
