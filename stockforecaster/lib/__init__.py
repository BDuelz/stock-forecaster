from beaker.cache import CacheManager
from stockforecaster.lib.utils import Storage


cache = CacheManager()

consts = Storage()

# Message types
consts.INFO = 'info'
consts.SUCCESS = 'success'
consts.WARNING = 'warning'
consts.ERROR = 'error'