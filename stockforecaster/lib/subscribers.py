import datetime
from pyramid.events import BeforeRender, subscriber
from stockforecaster.config import settings
from stockforecaster.lib import consts
from stockforecaster.lib.utils import Storage


@subscriber(BeforeRender)
def add_renderer_globals(event):
    '''
    Pass variables to the template.
    '''
    event['g'] = Storage(consts, settings=settings)
    event['h'] = Storage(datetime=datetime)
