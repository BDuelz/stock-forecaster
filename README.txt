Stock Forecaster.

By: Abdul Hassan <abdulrhass@gmail.com>
Date: 04/26/2013
Version: 1.1

--------------------------------------------------------------------------------

Copyright

(c) 2013 Abdul Hassan. All rights reserved.

--------------------------------------------------------------------------------

Dependencies

The main dependency is Vagrant (http://www.vagrantup.com) and all it's dependencies,
as well as fabric (http://docs.fabfile.org/en/1.6). After that, everything else 
is downloaded and installed automatically from the fabfile.

--------------------------------------------------------------------------------

Installation

Run `fab vagrant development deploy` to install and set up the virtual machine.

--------------------------------------------------------------------------------

Usage

From a browser, visit http://10.10.10.10

--------------------------------------------------------------------------------

Authors

Abdul Hassan <abdulrhass@gmail.com>

--------------------------------------------------------------------------------