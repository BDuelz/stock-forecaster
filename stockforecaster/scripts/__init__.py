import argparse
import sys
from pyramid.paster import bootstrap
from stockforecaster.config import settings
from stockforecaster.lib.utils import Storage


def make_main(Command):
    def main(argv=None):
        if argv is None:
            argv = sys.argv
        command = Command(argv)
        sys.exit(command.run())
    return main


class BaseCommand(object):

    parser = argparse.ArgumentParser()
    parser.add_argument('config_uri', action='store', help='Configuration file to use')

    def __init__(self, argv):
        '''
        Setup the necessary environment to run the script.
        '''
        self.args = self.parser.parse_args(argv[1:])
        try:
            self.env = Storage(bootstrap(self.args.config_uri))
        except Exception as e:
            self.env = None
            raise SystemExit(e)
        self.settings = settings

    def __del__(self):
        if self.env is not None:
            self.env.closer()

    def run(self):
        raise NotImplementedError()
